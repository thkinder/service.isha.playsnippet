With this plugin it is possible to play only a part of the video which
can be defined in a json formatted configuration file.

For example if we only want to play a 10 minutes video from minute 5 to
minute seven we can store the following config.json file in the same directory
where the video is stored.

```json
{
  "MyVideFIleName":{
    start:"00:05:00",
    end:"00:07:00"
  }
}
```

How the script works:
Whenever a video is started, the service script is being called.
The script reads the location of the currently played video and looks if it
can find a config.json file in this directory. If it does not find any json
file it will not do anything.

If it find a config.json file it will read it, and looks if it finds an entry
with the exact name as the file name being played. In case the match is true
it will seek to the specific "start" time. Then it checks in 1s interval if the
end time has reached. If so it will seek to the end of the video.

Seeking to the end is done so that it will behave like any other video,
for example in case we use it in a playlist.
