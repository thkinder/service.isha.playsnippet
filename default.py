import xbmc
import xbmcaddon
import xbmcgui
import os
import json


class PlaySnippet(xbmc.Player):
    CFG_FILE_EXTENSION = ".cfg"
    KEY_START = "start"
    KEY_END = "end"
    debug = True
    runnig = False
    dict = {}
    tStart = -1;
    tEnd = -1;

    def __init__(self):
        xbmc.Player.__init__(self)

    def logNotice(self, msg):
        if self.debug:
            xbmc.log(msg=msg, level=xbmc.LOGNOTICE);

    def onPlayBackStarted(self):
        
        if self.chkConfig() == False:
            return

        xbmc.Player().seekTime(self.getStartInSec())
        self.runnig = True

    def getStartInSec(self):
        return self.getTimeInSec(self.dict[self.KEY_START])

    def getEndInSec(self):
        return self.getTimeInSec(self.dict[self.KEY_END])


    def getTimeInSec(self, time):

        tmp = time.split(":")

        sec = int(tmp[2])
        min_s = int(tmp[1]) * 60
        hour_s = int(tmp[0]) * 3600

        return sec + min_s + hour_s


    def isTimeFormat(self, time):
        tmp = time.split(":")

        if len(tmp) != 3:
            self.logNotice("length is not 3 not time format... {l}".format(l=len(tmp)))
            return False

        if int(tmp[2]) > 59 :
            self.logNotice("isTimeFormat::seconds out of range")
            return False
        elif int(tmp[1]) > 59 :
            self.logNotice("isTimeFormat::minutes out of range")
            return False
        elif  int(tmp[0]) > 99:
            self.logNotice("isTimeFormat::hours out of range")
            return False

        return tmp


    def chkTimeValueGreater(self, tStart, tEnd):


        #Check if valid time values have been assigned
        startIsTime =  self.isTimeFormat(tStart)
        endIsTime =  self.isTimeFormat(tEnd)

        self.tStart = tStart
        self.tEnd = tEnd

        if startIsTime == False  or endIsTime == False:
            self.logNotice("chkTimeValue:: start and end are not time values")

            return False

        #compare time and see that start is grater then end

        if int(endIsTime[0]) > int(startIsTime[0]):
            return True
        elif int(endIsTime[1]) > int(startIsTime[1]):
            return True
        elif  int(endIsTime[2]) > int(startIsTime[2]):
            return True

        return False
        # if int(startIsTime[0]) > int(endIsTime[0]): #cmp hours
        #     self.logNotice("chkTimeValue:: start time not smaller..." )
        #     self.logNotice("1::{s} {e}".format(s=int(startIsTime[0]), e =  int(endIsTime[0]) ) )
        #     return False
        #
        #
        # elif int(startIsTime[1]) > int(endIsTime[1]): #cmp hours
        #     self.logNotice("chkTimeValue:: start time not smaller..." )
        #     self.logNotice("2::{s} {e}".format(s=int(startIsTime[1]), e =  int(endIsTime[1]) ) )
        #     return False
        #
        # elif int(startIsTime[2]) > int(endIsTime[2]): #cmp hours
        #     self.logNotice("chkTimeValue:: start time not smaller..." )
        #     self.logNotice("3::{s} {e}".format(s=int(startIsTime[2]), e =  int(endIsTime[2]) ) )

            #self.logNotice("{i} {s} {e}".format(i=i, s=int(startIsTime[i]), e =  int(endIsTime[i]) ) )
            #return False




        #return True

    def chkConfig(self):
        nameOrig = xbmc.Player().getPlayingFile()
        name = nameOrig.split('.')
        data = ""

        if len(name) < 2:
            self.logNotice("File name does not have a file extension. stop... {n}".format(n=name))
            return False

        path = os.path.join(name[0]+self.CFG_FILE_EXTENSION)
        jsonPath = os.path.join(os.path.dirname(nameOrig),"config.json")

        try:
            fd = open(jsonPath, "r")
            if os.path.isfile(jsonPath):
                data = json.load(fd)
            else:
                #self.logNotice("no config file found::checked:: {path}".format(path=path))
                return False

            jFile = data[os.path.basename(nameOrig)]
            self.logNotice(str(jFile))

        except Exception as e:
            #self.logNotice("File does not have a config entry/file not found...")
            return False

        if (not jFile['start']) or (not jFile['end']):
            self.logNotice("Start or end key not defined properly...")
            return False

        self.dict = {self.KEY_START:jFile['start'], self.KEY_END:jFile['end']}

        #Handle input -1 which means start and end of file
        try:
            if int(self.dict[self.KEY_START]) == -1:
                self.logNotice("start is -1")
                self.dict[self.KEY_START] = "00:00:00"

        except:
            pass

        try:
            if int(self.dict[self.KEY_END]) == -1:
                #tEnd = tCur = xbmc.Player().getTime()
                tEnd = int(xbmc.Player().getTotalTime())
                self.logNotice("tend = " + str(tEnd))

                hour = tEnd // 3600
                tEnd -= (hour * 3600)

                min = tEnd // 60
                tEnd -= min * 60

                self.dict[self.KEY_END] = "{:02d}:{:02d}:{:02d}".format(hour, min, tEnd)
        except:
            pass

        #When we came here config file exists, and keys are correct, now we
        #need to check time format for start and end
        tmp = self.chkTimeValueGreater(self.dict[self.KEY_START], self.dict[self.KEY_END])
        if tmp == False:
            self.logNotice("Start time is not grater then end time, or time values are not defined in file...")
            self.logNotice(str(self.dict))
            return False

        return True



if __name__ == "__main__":

    snip = PlaySnippet()
    snip.debug = True #Todo: replace constant True with value from config file
    snip.logNotice("\n\nStart script\n\n")


    while(not xbmc.abortRequested):
        xbmc.sleep(1000)

        if xbmc.Player().isPlaying():
            tCur = xbmc.Player().getTime()

            if snip.runnig and tCur >= snip.getEndInSec():
                snip.logNotice("End of playback detected,seek to the end of file....")
                xbmc.Player().seekTime(xbmc.Player().getTotalTime())
                snip.runnig = False
